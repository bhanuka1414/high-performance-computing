/****************************************************************************
  This version adds five coins to the account. How long will it take to run?
  How much money will eventually be stored in the account?
  
  Compile with:

    cc -o penny03 penny03.c time_diff.c time_diff.c -lrt

*****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include "time_diff.h"

void add_penny(int *balance) {
  int b = *balance;// assigning pointer

// 1 second delay (simulating large calculation time)

  usleep(1000000);

  b = b + 1;
  *balance = b;
}

int main(){
  struct timespec start, finish;
  int i;//assigning varible
  long long int difference;   
  int account = 0;//assigning varible
  clock_gettime(CLOCK_MONOTONIC, &start);

  for(i=0;i<5;i++){
    add_penny(&account);
  }

  clock_gettime(CLOCK_MONOTONIC, &finish);
  time_difference(&start, &finish, &difference);

  printf("accumulated %dp\n", account);//printning accumulated     
  printf("run lasted %lldns or %9.5lfs\n", difference, difference/1000000000.0);// printnig the runlasted
  return 0;
} 
